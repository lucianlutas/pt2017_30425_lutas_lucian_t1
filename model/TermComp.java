package ro.tuc.pt.asig1.model;

import java.util.Comparator;

public class TermComp implements Comparator<Term>{

	@Override
	public int compare(Term t1, Term t2) {
		return t2.degree-t1.degree;
	}

}
