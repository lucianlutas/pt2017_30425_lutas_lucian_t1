package ro.tuc.pt.asig1.model;

import java.text.DecimalFormat;

public class Term {
	public double coef;
	public int degree;
	
	DecimalFormat nFormat = new DecimalFormat("0.##");
	
	public Term(double coef, int degree) {
		super();
		this.coef = coef;
		this.degree = degree;
	}
	
	public String toString(){
		String str = "";
		if(coef > 0){
			if(degree == 0)
				str = " + " + nFormat.format(coef);
			else
				str = " + " + nFormat.format(coef) + "*x" + "^" + degree;
		} else if(coef < 0){
			if(degree == 0)
				str = " - " + nFormat.format(-coef);
			else
				str = " - " + (nFormat.format(-coef)) + "*x" + "^" + degree;
		} else if(coef == 0){
			str = "";
		}
		return str;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Term other = (Term) obj;
		if (Double.doubleToLongBits(coef) != Double.doubleToLongBits(other.coef))
			return false;
		if (degree != other.degree)
			return false;
		if (nFormat == null) {
			if (other.nFormat != null)
				return false;
		} else if (!nFormat.equals(other.nFormat))
			return false;
		return true;
	}
}
