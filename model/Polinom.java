package ro.tuc.pt.asig1.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Polinom {
	public List<Term> term;
	
	public Polinom (){
		term = new ArrayList<Term>();
	}
	
	public void addTerm(Term t){
		boolean br = false;
		for(Term d:term){
			if(t.degree == d.degree){
				d.coef = d.coef + t.coef;
				br = true;
			}
		}
		
		if(!br){
			term.add(t);
		}
		Collections.sort(term, new TermComp());
	}
	
	public String toString(){
		String str = "";
		for(Term t:term){
			str += t.toString();
		}
		return str;
		
	}
	
	public Polinom copyPol(){
		
		Polinom copy = new Polinom();
		
		for(Term i:term){
			copy.addTerm(new Term(i.coef, i.degree));
		}
		return copy;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Polinom other = (Polinom) obj;
		if (term == null) {
			if (other.term != null)
				return false;
		} else if (!term.equals(other.term))
			return false;
		return true;
	}
}










