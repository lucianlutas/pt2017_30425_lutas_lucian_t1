package ro.tuc.pt.asig1.test;

import static org.junit.Assert.*;

import java.util.Collections;

import org.junit.Before;
import org.junit.Test;

import ro.tuc.pt.asig1.logic.Operations;
import ro.tuc.pt.asig1.model.Polinom;
import ro.tuc.pt.asig1.model.Term;
import ro.tuc.pt.asig1.model.TermComp;

public class Testing {

	Polinom p1;
	Polinom p2;
	Polinom expectedResult;
	
	@Before
	public void init(){
		p1 = new Polinom();
		Term t1 = new Term(3, 2);
		Term t2 = new Term(2, 1);
		Term t3 = new Term(1, 0);

		p1.addTerm(t1);
		p1.addTerm(t2);
		p1.addTerm(t3);
		
		p2 = new Polinom();
		Term t4 = new Term (5,2);
		Term t5 = new Term (4,1);
		Term t6 = new Term (6,0);
		
		p2.addTerm(t4);
		p2.addTerm(t5);
		p2.addTerm(t6);
			
	}
	
	@Test
	public void testAdd() {
		Polinom actualResult = Operations.addOp(p1, p2);
		Polinom expectedResult = new Polinom(); 
		Term f1 = new Term (8, 2);
		Term f2 = new Term (6, 1);
		Term f3 = new Term (7, 0);
		expectedResult.addTerm(f1);
		expectedResult.addTerm(f2);
		expectedResult.addTerm(f3);
		Collections.sort(actualResult.term, new TermComp());
		Collections.sort(expectedResult.term, new TermComp());
		System.out.println("Add");
		System.out.println(actualResult);
		System.out.println(expectedResult+ "\n");
		assertEquals(expectedResult, actualResult);
	}
	@Test
	public void testSub() {
		Polinom actualResult = Operations.subOp(p2, p1);
		Polinom expectedResult = new Polinom(); 
		Term f1 = new Term (2, 2);
		Term f2 = new Term (2, 1);
		Term f3 = new Term (5, 0);
		expectedResult.addTerm(f1);
		expectedResult.addTerm(f2);
		expectedResult.addTerm(f3);
		Collections.sort(actualResult.term, new TermComp());
		Collections.sort(expectedResult.term, new TermComp());
		System.out.println("Sub");
		System.out.println(actualResult);
		System.out.println(expectedResult+ "\n");
		assertEquals(expectedResult, actualResult);
	}
	
	@Test
	public void testMul() {
		Polinom actualResult = Operations.mulOp(p1, p2);
		Polinom expectedResult = new Polinom(); 
		Term f1 = new Term (15, 4);
		Term f2 = new Term (22, 3);
		Term f3 = new Term (31, 2);
		Term f4 = new Term (16, 1);
		Term f5 = new Term (6, 0);
		expectedResult.addTerm(f1);
		expectedResult.addTerm(f2);
		expectedResult.addTerm(f3);
		expectedResult.addTerm(f4);
		expectedResult.addTerm(f5);
		Collections.sort(actualResult.term, new TermComp());
		Collections.sort(expectedResult.term, new TermComp());
		System.out.println("Mul");
		System.out.println(actualResult);
		System.out.println(expectedResult+ "\n");
		assertEquals(expectedResult, actualResult);
	}
	@Test
	public void testInt() {
		Polinom actualResult = Operations.integralOp(p1);
		Polinom expectedResult = new Polinom(); 
		Term f1 = new Term (1, 3);
		Term f2 = new Term (1, 2);
		Term f3 = new Term (1, 1);
		expectedResult.addTerm(f1);
		expectedResult.addTerm(f2);
		expectedResult.addTerm(f3);
		Collections.sort(actualResult.term, new TermComp());
		Collections.sort(expectedResult.term, new TermComp());
		System.out.println("Int");
		System.out.println(actualResult);
		System.out.println(expectedResult+ "\n");
		assertEquals(expectedResult, actualResult);
	}
	@Test
	public void testDiff() {
		Polinom actualResult = Operations.diffOp(p1);
		Polinom expectedResult = new Polinom(); 
		Term f1 = new Term (6, 1);
		Term f2 = new Term (2, 0);
		expectedResult.addTerm(f1);
		expectedResult.addTerm(f2);
		Collections.sort(actualResult.term, new TermComp());
		Collections.sort(expectedResult.term, new TermComp());
		System.out.println("Diff");
		System.out.println(actualResult);
		System.out.println(expectedResult+ "\n");
		assertEquals(expectedResult, actualResult);
	}

}
