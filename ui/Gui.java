package ro.tuc.pt.asig1.ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

import ro.tuc.pt.asig1.logic.Operations;
import ro.tuc.pt.asig1.model.Polinom;

public class Gui {
	public static JButton btn1;
	public static JButton btn2;
	public static JButton btnAdd;
	public static JButton btnSub;
	public static JButton btnMul;
	public static JButton btnInt;
	public static JButton btnDiff;
	public static JTextField textFieldP1;
	public static JTextField textFieldP2;
	public static JTextField textFieldResult;
	public JLabel lblRes;
	
	public Gui() {
		JFrame window = new JFrame("Polynomials");
		window.setSize(500, 500);
		window.setLocation(100,100);
		window.setLayout(null);
		window.setBounds(0, 0, 500, 500);
		
		textFieldP1 = new JTextField("3x^3-5x^2+10x^1-3x^0");
		textFieldP2 = new JTextField("3x^1 + 1x^0");
		textFieldResult = new JTextField();
		window.add(textFieldP1);
		window.add(textFieldP2);
		window.add(textFieldResult);
		textFieldP1.setBounds(50, 65, 190, 25);
		textFieldP2.setBounds(260, 65, 190, 25);
		textFieldResult.setBounds(180, 400, 190, 25);
		
		btn1 = new JButton("Set Polynome 1");
		btn2 = new JButton("Set Polynome 2");
		btnAdd = new JButton("P1 + P2");
		btnSub = new JButton("P1 - P2");
		btnMul = new JButton("P1 * P2");
		btnInt = new JButton("Integrate P1");
		btnDiff = new JButton("Differentiate P1");
		window.add(btn1);
		window.add(btn2);
		window.add(btnAdd);
		window.add(btnSub);
		window.add(btnMul);
		window.add(btnInt);
		window.add(btnDiff);
		btn1.setBounds(70, 100, 150, 50);
		btn2.setBounds(280, 100, 150, 50);
		btnAdd.setBounds(70, 160, 150, 50);
		btnSub.setBounds(280, 160, 150, 50);
		btnMul.setBounds(70, 220, 150, 50);
		btnInt.setBounds(280, 220, 150, 50);
		btnDiff.setBounds(175, 300, 150, 50);
		
		JLabel lblRes = new JLabel("Result: ");
		window.add(lblRes);

		lblRes.setBounds(130, 387, 150, 50);

		
		window.setVisible(true);
		window.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
	}
	
	public static void btnP1ActionListener (Polinom p1){
		btn1.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				Operations.clear(p1);
				Operations.setInstance(p1, textFieldP1.getText().toString());
				textFieldP1.setText(p1.toString());
				
			}
		});
	}
	public static void btnP2ActionListener (Polinom p2){
		btn2.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				Operations.clear(p2);
				Operations.setInstance(p2, textFieldP2.getText().toString());
				textFieldP2.setText(p2.toString());
				
			}
		});
	}
	
	public static void btnAddActionListener (Polinom p1, Polinom p2){
		btnAdd.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				Polinom res = new Polinom();
				res = Operations.addOp(p1, p2);
				textFieldResult.setText(res.toString());
				
			}
		});
	}
	
	public static void btnSubActionListener (Polinom p1, Polinom p2){
		btnSub.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				Polinom res = new Polinom();
				res = Operations.subOp(p1, p2);
				textFieldResult.setText(res.toString());
				
			}
		});
	}
	public static void btnMulActionListener (Polinom p1, Polinom p2){
		btnMul.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				Polinom res = new Polinom();
				res = Operations.mulOp(p1, p2);
				textFieldResult.setText(res.toString());
				
			}
		});
	}
	public static void btnIntActionListener (Polinom p1){
		btnInt.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				Polinom res = new Polinom();
				res = Operations.integralOp(p1);
				textFieldResult.setText(res.toString());
				
			}
		});
	}
	
	public static void btnDiffActionListener (Polinom p1){
		btnDiff.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				Polinom res = new Polinom();
				res = Operations.diffOp(p1);
				textFieldResult.setText(res.toString());
				
			}
		});
	}
	
}
