package ro.tuc.pt.asig1.logic;

import ro.tuc.pt.asig1.model.Polinom;
import ro.tuc.pt.asig1.ui.Gui;

public class Main {

	public static void main(String[] args) {
		
		Gui gui = new Gui();
		
		Polinom p1 = new Polinom();
		Polinom p2 = new Polinom();

		
		Gui.btnP1ActionListener(p1);
		Gui.btnP2ActionListener(p2);
		Gui.btnAddActionListener(p1, p2);
		Gui.btnSubActionListener(p1, p2);
		Gui.btnMulActionListener(p1, p2);
		Gui.btnIntActionListener(p1);
		Gui.btnDiffActionListener(p1);
	
		
		
	}

}
