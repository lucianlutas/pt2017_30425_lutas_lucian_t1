package ro.tuc.pt.asig1.logic;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ro.tuc.pt.asig1.model.Polinom;
import ro.tuc.pt.asig1.model.Term;

public class Operations {
	
	public static Polinom addOp(Polinom p1, Polinom p2){
		Polinom res = new Polinom();
		res = p2.copyPol();
		for(Term t1:p1.term){
			res.addTerm(t1);
		}
		return res;
	}
	
	public static Polinom subOp(Polinom p1, Polinom p2){
		
		Polinom res = new Polinom();
		res = p1.copyPol();
		for(Term t:p2.term){
			boolean br = false;
			for(Term d:res.term){
				if(t.degree == d.degree){
					d.coef = d.coef - t.coef;
					br = true;
				}
			}
			
			if(!br){
				res.term.add(t);
			}
		}
		return res;
		
	}
	
	public static Polinom mulOp(Polinom p1, Polinom p2){
		
		Polinom res = new Polinom();
		Polinom t1 = new Polinom();
		Polinom t2 = new Polinom();
		t1 = p1.copyPol();
		t2 = p2.copyPol();
		for(Term d:t1.term){
			for(Term t:t2.term){
				Term f = new Term(d.coef*t.coef, d.degree+t.degree);
				res.addTerm(f);
			}
		}
		return res;
	}
	public static Polinom divOp(Polinom p1, Polinom p2){
		
		Polinom res = new Polinom();
		Polinom t1 = new Polinom();
		Polinom t2 = new Polinom();
		t1 = p1.copyPol();
		t2 = p2.copyPol();
		for(Term d:t1.term){
			for(Term t:t2.term){
				Term f = new Term(d.coef/t.coef, d.degree-t.degree);
				res.addTerm(f);
			}
		}
		return res;
	}
	
	public static Polinom integralOp(Polinom p1){
		Polinom temp = new Polinom();
		temp = p1.copyPol();
		for(Term d:temp.term){
			d.coef = d.coef / (d.degree + 1);
			d.degree += 1;
		}
		
		return temp;
	}
	
	public static Polinom diffOp(Polinom p1){
		Polinom temp = new Polinom();
		temp = p1.copyPol();
		for(Term d:temp.term){
			d.coef = d.coef * d.degree;
			d.degree -= 1;
		}
		
		return temp;
	}
	
	public static Polinom setInstance(Polinom p1, String input){
		Pattern p = Pattern.compile( "(-?\\b\\d+)[xX]\\^(-?\\d+\\b)" );
		Matcher m = p.matcher( input );
		while (m.find()) {
		    Term t = new Term(Double.parseDouble(m.group(1)),Integer.parseInt(m.group(2)));
		    p1.addTerm(t);
		}
		return p1;
	}
	
	
	public static Polinom clear(Polinom p1){
		
		for(Term t:p1.term){
			t.coef = 0;
			t.degree = 0;
		}
		
		return p1;
	}
	
}
